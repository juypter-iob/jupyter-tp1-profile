# Jupyter TP1 Profile


## Setup local environment
Setup environment for local work with TP1 jupyter notebooks

### Windows 10

1. Install Anaconda 64 bit from https://www.anaconda.com/products/individual (and make anaconda be found in path)
2. Install Julia from https://julialang.org/downloads/
3. Add Julia bin folder to the users PATH environment variable 
4. Run Julia requirements.jl in folder with CMD (or powershell) with "julia requirements.jl" (or run "include("requirements.jl")" inside Julia REPL - you must be in the same folder or give the complete path of requirement.jl)
5. Start notebook from CMD (or Powershell) with "julia start_notebook.jl"
6. To end notebook use CTRL+C in Console
7. 

The code is licensed under the [MIT license](https://opensource.org/licenses/MIT).
